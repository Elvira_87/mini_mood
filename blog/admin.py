from django.contrib import admin
from blog.models import Product, Photo, News, Menu

admin.site.register(Product)
admin.site.register(Photo)
admin.site.register(News)
admin.site.register(Menu)
