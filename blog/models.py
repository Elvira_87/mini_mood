from django.conf import settings
from django.db import models
from django.utils import timezone


class Product(models.Model):
    photos = models.ImageField(upload_to='photo_product', blank=True, null=True)
    code = models.CharField(verbose_name='Артикул', max_length=128,blank=True, null=True)
    category = models.CharField(max_length=128,blank=True, null=True)
    name = models.CharField(verbose_name='Наименование', max_length=128, blank=True, null=True)
    size = models.CharField(verbose_name='Размер', max_length=128, blank=True, null=True)
    made_by = models.CharField(verbose_name='Возрастная группа', max_length=128, blank=True, null=True)
    color = models.CharField(verbose_name='Цвет', max_length=128, blank=True, null=True)
    price = models.CharField(verbose_name='Цена за единицу', max_length=128, blank=True, null=True)


class Photo(models.Model):
    photo = models.ImageField(upload_to='photo_photo')
    


class News(models.Model):
    photo = models.ImageField(upload_to='photo_models')
    title =  models.CharField(max_length=128)
    link = models.URLField(verbose_name=None)


class Menu(models.Model):
    photo = models.ImageField(upload_to='photo_menu')
    title =  models.CharField(max_length=128)
    link = models.URLField(verbose_name=None, blank=True, null=True)
