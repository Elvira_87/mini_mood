from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('girls/', views.girls, name='girls'),
    path('boys/', views.boys, name='boys'),
    path('show/<int:pk>/', views.show, name='show'),
    path('dostavka/', views.dostavka, name='dostavka'),
]
