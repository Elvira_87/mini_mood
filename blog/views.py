from django.shortcuts import render
from .models import Product, Photo, Menu, News
from django.utils import timezone


def index(request):
    posts = Product.objects.all()
    photo = Photo.objects.all()
    menu = Menu.objects.all()
    news = News.objects.all()
    return render(request, 'blog/index.html', {'photos': photo, 'menus': menu, 'newss': news})

def girls(request):
    posts = Product.objects.filter(category='girl')
    return render(request, 'blog/girls.html', {'posts': posts})

def boys(request):
    posts = Product.objects.filter(category='boy')
    return render(request, 'blog/boys.html', {'posts': posts})

def show(request, pk):
    posts = Product.objects.get(pk=pk)
    return render(request, 'blog/show.html', {'post': posts})

def dostavka(request):
    posts = Product.objects.all()
    return render(request, 'blog/dostavka.html', {})
